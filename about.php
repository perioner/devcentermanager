﻿<?php
include("app/config/database.php");
$staff = mysql_query("SELECT * FROM `staff` ORDER BY `idstaff` ASC limit 6") or die (mysql_error()); 
$menu = mysql_query("SELECT * FROM `menu_url` ORDER BY `idmenu` ASC limit 20") or die (mysql_error()); 


?>



<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Darksoldiers" >

    <title>Games Proyect | Sobre Nosotros</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="css/gamesproyect.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Ocultar Navegacion</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Games Proyect</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                   <?php
while($menu1 = mysql_fetch_assoc($menu)) 
{ 
  echo ' 
  

              <li><a href="'.htmlspecialchars($menu1['url']).'">'.htmlspecialchars($menu1['Nombre']).'</a></li>
   
  ';
} 
?> 
             
            </div>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="container">

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Sobre Nosotros 
                    <small>Enterate de quién compone nuestro equipo !</small>
                </h1>
           
            </div>

        </div>

        <div class="row">

            <div class="col-md-6">
            </div>
            <div class="col-md-6">
                <h2>Bienvenido a Games Proyect</h2>
                <p>Games Proyect esta compuesto por un Equipo de Profesionales los cuales hacen posible el correcto funcionamiento de cada servidor
                 que creamos o abrimos al publico .</p>
                <p>Intentando mantener asi nuestra rapida asistencia tecnica y mejorando dia a dia nuestros servicios ! </p>
            </div>

        </div>

        <!-- Team Member Profiles -->

        <div class="row">

            <div class="col-lg-12">
                <h2 class="page-header">Nuestro Equipo</h2>
            </div>
             <?php
while($staff1 = mysql_fetch_assoc($staff)) 
{ 
  echo ' 
  
 
            <div class="col-sm-4">
 
 <img class="img-circle img-responsive" src="http://placehold.it/200x200">
     <h3>'.htmlspecialchars($staff1['staff']).'
                    <small>'.htmlspecialchars($staff1['cargo']).'</small>
                </h3>
      <p> '.htmlspecialchars($staff1['descripcion']).' .</p>
	    <ul class="list-unstyled list-inline list-social-icons">
                    <li class="tooltip-social facebook-link"><a href="'.htmlspecialchars($staff1['fbUrl']).'" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook-square fa-2x"></i></a>
                    </li>
   
                    <li class="tooltip-social twitter-link"><a href="'.htmlspecialchars($staff1['twurl']).'" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter-square fa-2x"></i></a>
                    </li>
                 
                </ul>
            </div> 
  ';
} 
?> 
          

        </div>

        <!-- Our Customers -->

        
    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Games Proyect 2014</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/devcenter.js"></script>

</body>

</html>
