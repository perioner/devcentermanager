<?php
include("../app/config/database.php");
//creamos la sesion
session_start();

//validamos si se ha hecho o no el inicio de sesion correctamente

//si no se ha hecho la sesion nos regresará a login.php
if(!isset($_SESSION['email'])) 
{
  header('Location: index.php'); 
  exit();
}


if($_GET['mandar']=="valnews") {

/*caturamos nuestros datos que fueron enviados desde el formulario mediante el metodo POST
**y los almacenamos en variables.*/
 $fecha1=$_POST['fecha'];
 $autor1=$_POST['autor'];
 $imagen1=$_POST['imagen'];
 $tituloa=$_POST['titulo'];
 $mensajec=$_POST['cuerpo1'];
 $mensajelargo=$_POST['cuerpo2'];

/* Generamos la Consulta de la BD
** Y la enviamos a la Base de Datos.*/


$sql="INSERT INTO blog (`titulo`, `images`, `autor`, `mensaje`, `fecha`,  `mensajelargo`) VALUES ('$tituloa', '$imagen1', '$autor1', '$mensajec', '$fecha1', '$mensajelargo')";  
	   mysql_query($sql, $conecta) or die ("Error al insertar datos ". mysql_error());
	   
	    header('Location: dashboard.php'); 
		exit();
	  }

/*Mysql_close() se usa para cerrar la conexión a la Base de datos y es 
**necesario hacerlo para no sobrecargar al servidor, bueno en el caso de
**programar una aplicación que tendrá muchas visitas ;) .*/
mysql_close();
?> 


<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>DashBoard | Admin Panel</title>

    <!-- Core CSS - Include with every page -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Blank -->

    <!-- SB Admin CSS - Include with every page -->
    <link href="css/sb-admin.css" rel="stylesheet">
  <script>
function revisar() {
if(formulario.fecha == "") { alert('Debes poner la Fecha') ; return false ; }
if(formulario.autor == "") { alert('Debes poner el autor') ; return false ; }
if(formulario.imagen == "") { alert('Debes poner una Url de una Imagen') ; return false ; }
if(formulario.titulo == "") { alert('Debes poner el titulo de noticia') ; return false ; }
if(formulario.cuerpo1 == "") { alert('Debes poner el mensaje corto') ; return false ; }
if(formulario.cuerpo2 == "") { alert('Debes poner el mensaje largo') ; return false ; }
}
</script>
	
	
</head>

<body>

    <div id="wrapper">

          <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Admin DashBoard | Games Proyect 2.0</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
               
               
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> Perfil Admin</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Configuracion Admin</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Desconectar (ACP)</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

        </nav>
        <!-- /.navbar-static-top -->

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                    </li>
                    <li>
                        <a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Inicio</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Blog<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level">
                            <li>
                                <a href="addnew.php">Añadir Entrada</a>
                            </li>
                            <li>
                                <a href="news.php">Noticias Publicadas</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href=""><i class="fa fa-table fa-fw"></i> Usuarios</a>
                    </li>
                 <li>
                        <a href="cmenu.php"><i class="fa fa-table fa-fw"></i> Configurar Menu</a>
                    </li>
                            <li>
                                <a href="salir.php">Cerrar Sessión</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- /.navbar-static-side -->

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Añadir Noticias</h1>
                </div>
				<!-- /.col-lg-12 -->
				
				<div class="col-lg-6">
                                      <form role="form" action="addnew.php?mandar=valnews" method="post">
					
                                        <div class="form-group">
                                            <label>Imagen [URL]</label>
                                            <input class="form-control" name="imagen">
                                            <p class="help-block">url de la imagen.</p>
                                        </div>
										 <div class="form-group">
                                            <label>Titulo del Mensaje</label>
                                            <input class="form-control" name="titulo">
                                            <p class="help-block">Titulo </p>
                                        </div>
                                        <div class="form-group">
                                            <label>Autor</label>
                                            <input class="form-control" placeholder="Enter text" name="autor">
                                        </div>
                                      
                                      <div class="form-group">
                                            <label>Fecha</label>
                                            <input class="form-control" placeholder="Enter text" name="fecha">
                                        </div>
										
										 <div class="form-group">
                                            <label>Mensaje Corto</label>

                                            <textarea class="form-control" rows="3" name="cuerpo1"></textarea>                                          
										  </div>
                                        <div class="form-group">
                                            <label>Mensaje Largo</label>
                                            <textarea class="form-control" rows="3" name="cuerpo2"></textarea>
                                        </div>
                                      
                                       
                                      
                                     
                                        <button type="submit" class="btn btn-default" action="dashboard.php" >Mandar Noticia</button>
                                        <button type="reset" class="btn btn-default">Resetear</button>
                                    </form>
									
                                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metismenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Blank -->

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Blank - Use for reference -->

</body>

