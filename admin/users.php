<?php
include("../app/config/database.php");
//creamos la sesion
session_start();

//validamos si se ha hecho o no el inicio de sesion correctamente

//si no se ha hecho la sesion nos regresará a login.php
if(!isset($_SESSION['email'])) 
{
  header('Location: index.php'); 
  exit();
}

// listado de noticias
// traemos listado de noticias
$arrUsers = array();
$query = "SELECT id, usuario, email, pin, account, email FROM `usuarios` WHERE id ORDER BY id DESC limit 20";
$resultado = mysql_query ($query, $conecta);
while ( $row = mysql_fetch_assoc ($resultado)) {
    array_push( $arrUsers,$row );
}
 
?> 


<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>DashBoard | Admin Panel</title>

    <!-- Core CSS - Include with every page -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Blank -->

    <!-- SB Admin CSS - Include with every page -->
    <link href="css/sb-admin.css" rel="stylesheet">
	
	
</head>

<body>

    <div id="wrapper">

          <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Admin DashBoard | Games Proyect 2.0</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
               
               
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> Perfil Admin</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Configuracion Admin</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Desconectar (ACP)</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

        </nav>
        <!-- /.navbar-static-top -->

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                    </li>
                    <li>
                        <a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Inicio</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Blog<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                            <li>
                                <a href="addnew.php">Añadir Entrada</a>
                            </li>
                            <li>
                                <a href="news.php">Noticias Publicadas</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="users.php"><i class="fa fa-table fa-fw"></i> Usuarios</a>
                    </li>
                 <li>
                        <a href="cmenu.php"><i class="fa fa-table fa-fw"></i> Configurar Menu</a>
                    </li>
                            <li>
                                <a href="salir.php">Cerrar Sessión</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- /.navbar-static-side -->

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Administrador de Usuarios</h1>
                </div>
				<!-- /.col-lg-12 -->
				
				<div class="col-lg-6">
<div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Usuario</th>
                                            <th>Email</th>
                                            <th>Pin</th>
                                            <th>Estado Cuenta</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									
										   <? foreach ( $arrUsers as $usuarios ) { ?>
                                        <tr>
                                            <td><? echo $usuarios['id']; ?></td>
                                            <td><? echo $usuarios['usuario']; ?></td>
                                            <td><? echo $usuarios['email']; ?></td>
                                            <td><? echo $usuarios['pin']; ?></td>
                                            <td><? echo $usuarios['account']; ?></td>
                                            <td><a class="btn btn-primary" href="">Control de Usuario<span class="glyphicon glyphicon-chevron-right"></span></a> </td>
                                        </tr>
                                 
<? } ?>
                                    </tbody>
                                </table>
                            </div>
                                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metismenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Blank -->

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Blank - Use for reference -->

</body>

