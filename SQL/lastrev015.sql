-- phpMyAdmin SQL Dump
-- version 4.1.8
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 17-03-2014 a las 13:01:00
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `desarrol_area`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administradores`
--

CREATE TABLE IF NOT EXISTS `administradores` (
  `idadmin` int(4) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(15) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `rango` varchar(900) NOT NULL,
  PRIMARY KEY (`idadmin`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `administradores`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `idblog` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL DEFAULT 'No Hay Titulo',
  `autor` varchar(50) NOT NULL DEFAULT 'Desconocido',
  `mensaje` varchar(9000) NOT NULL DEFAULT 'No Hay Mensaje',
  `Fecha` date NOT NULL,
  `mensajelargo` varchar(3000) NOT NULL,
  PRIMARY KEY (`idblog`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `blog`
--

INSERT INTO `blog` (`idblog`, `titulo`, `autor`, `mensaje`, `Fecha`, `mensajelargo`) VALUES
(1, '[Desarrollo] Blog 100%', 'GamesProyect', '<p>Tenemos el blog ya al 100% desarrollado . </p>\n\nPara saber mas dale a Leer Mas\n\n\n<p><center>Atentamente : La Administracion de Games Proyect </center></p>', '2014-03-05', '<p>Tenemos el blog ya al 100% desarrollado . </p>\n\n<p><b>Ya Adelantamos las neuvas novedades de la Web y lo Proximo en salir .</p></b>\n<b> <p>Proximas Actualizaciones </b></p>\n<ul>\n<li> * Registro Arreglado y Mejorado </li>\n<li> * Panel de Usuario al 100%</li>\n<li> * Agregada la seccion de Proyectos </li>\n<li> * Agregada la Seccion de Tickets </li>\n</ul>\n\n<i> Eso es todo </i>\n\n\n<p><center>Atentamente : La Administracion de Games Proyect </center></p>'),
(2, '[Desarrollo] Blog Completado', 'GamesProyect', '<b><p> Estimados Usuarios </b></p>\r\n\r\n<b> Hemos terminado el sistema de Blog nuevo </b>\r\n\r\n<p> Para Saber mas dale a "Leer Mas" </p>', '2014-03-05', '<b><p> Estimados Usuarios </b> </p>\r\n\r\n<b><p> Nos complace informarles que hoy hemos abierto oficialmente el Blog </b></p>\r\n\r\n\r\n\r\n<p><b> Aun falta por desarrollar el sistema de comentarios pero ya pueden disfrutar de las ultimas novedades de Games Proyect </p></b>\r\n\r\n<i> Atentamente </i>\r\n\r\n<b> La Administracion de Games Proyect'),
(3, '[Desarrollo] Diseñando el Area de Usuario', 'GamesProyect', '<p>Estimados Usuarios </p>\n\n<b><p> A dia de hoy queremos centrarnos en el Dashboard del perfil de usuarios . Hemos decidido usar el mismo diseño de la pagina un poco adaptado y mejorado </b></p>\n\n<p>..... Dale a Leer Mas para mas Informacion </p>', '2014-03-06', '<p> No creo que haga falta decir que el desarrollo de otras funciones no esta olvidado </p>\r\n\r\n<p><b> Es posible que en unos dias hagamos un pequeño testeo con usuarios que de verdad quieran ayudar a la mejoria de Games Proyect </p></b>\r\n\r\n<p><i> En Games Proyect hemos hecho un calculo de espacío y personas online que podremos tener y hemos llegado a una referencia para la primera beta\r\nPodremos albergar 1000 Conexiones entrantes y 100 Proyectos sin ningun problema de Latencia del sitio web ni nada.</p></b> \r\n\r\n<p> Sin mas decirles que ya hemos arreglado las Ñ y las comas en nuestro sitio web y ya vuelven a reflejarse .</p>\r\n\r\n<p><b> Atentamente </p></b>\r\n\r\n<i> El Equipo de Desarrollo de GamesProyect </i>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE IF NOT EXISTS `comentarios` (
  `idComentario` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `comentario` varchar(255) NOT NULL,
  `idUsuario` int(11) unsigned NOT NULL,
  `idblog` int(11) unsigned NOT NULL,
  `estado` enum('sin validar','apto') NOT NULL DEFAULT 'apto',
  `fCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idComentario`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE IF NOT EXISTS `estado` (
  `Id` int(50) NOT NULL,
  `Nombre` varchar(500) NOT NULL,
  `Estado` varchar(5000) NOT NULL,
  `pin` varchar(500) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL DEFAULT 'No Hay Titulo',
  `autor` varchar(50) NOT NULL DEFAULT 'Desconocido',
  `mensaje` varchar(1000) NOT NULL DEFAULT 'No Hay Mensaje',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `autor`, `mensaje`) VALUES
(1, 'Demo Noticia', 'GamesProyect', '<b> Estimados Usuarios </b>\r\n\r\n<p> Aqui ira tu Noticia.</p>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `idstaff` int(10) NOT NULL AUTO_INCREMENT,
  `staff` varchar(255) NOT NULL,
  `cargo` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`idstaff`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `staff`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tickets`
--

CREATE TABLE IF NOT EXISTS `tickets` (
  `idticket` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(40) NOT NULL,
  `asunto` varchar(255) NOT NULL,
  `mensaje` varchar(255) NOT NULL,
  PRIMARY KEY (`idticket`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Volcado de datos para la tabla `tickets`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(15) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `usuarios`
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
